## Synopsis

A list of domains for existing Spotify AD's. This host file will assist user with blocking spotify ad's on their local networks or indivial devices.


## Motivation

Assist the users with setting up ad free music streaming and prevent unauthorized content from being executed. Advertisements can contain malicous code such as malware and spyway.

## Installation

Microsoft Windows 7, 8, 8.1, 10
C:/Windows/System32/drivers/etc/hosts

Linux (Debian, Ubuntu, CentOS, Redhat).
/etc/hosts

Android 5.X, 6.X, 7.X - Note symbolic links don't appear to work in 7.X
--Requires root permissions
/system/etc/hosts

Execute if on ready only filesystem: "mount -o remount,rw /system"


## Contributors

...

## License
Free to distribute. 